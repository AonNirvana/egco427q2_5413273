<?php
header("Access-Control-Allow-Origin: *");
header("Content-Type: application/json; charset=UTF-8");
header("Expires: Thu, 01 Dec 1983 20:00:00 GMT");

require_once './include/server.php';
$query = "SELECT * FROM `cardinfo`";
$result = mysqli_query($link, $query, $resultmode);
$array = array();
while ($row = mysqli_fetch_assoc($result)) {array_push($array, $row);}
mysqli_free_result($result);
mysqli_close($link);
$value = array('cardinfo'=>$array);
$json = json_encode($value) or die(json_last_error_msg());
echo $json;
?> 
