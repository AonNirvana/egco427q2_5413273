<?php require_once './login/success.php'; ?>
<?php
$host = "localhost";
$user = "egco427";
$pass = "egco427";
$database = "egco427_assign";

$link = mysqli_connect($host, $user, $pass, $database) or die("Could not connect to host.");

$query = "SELECT * FROM personinfo ORDER BY lastname";
$result = mysqli_query($link, $query) or die("Data not found.");

?> 
<html>
	<head>
		<meta content="text/html; charset=UTF-8" http-equiv="Content-Type"/>
			<title>In-class Example: PHP, MySQL, and XML</title>
			<style type="text/css">
					h3 {color:darkgreen}				
					b {color:darkgrey}
					i {color:darkblue}
			</style>
		<!-- Bootstrap Core CSS -->
                <link href="../bower_components/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">

                <!-- MetisMenu CSS -->
                <link href="../bower_components/metisMenu/dist/metisMenu.min.css" rel="stylesheet">

                <!-- Timeline CSS -->
                <link href="../dist/css/timeline.css" rel="stylesheet">

                <!-- Custom CSS -->
                <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

                <!-- Morris Charts CSS -->
                <link href="../bower_components/morrisjs/morris.css" rel="stylesheet">

                <!-- Custom Fonts -->
                <link href="../bower_components/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

                <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
                <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
                <!--[if lt IE 9]>
                    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
                    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>
                <![endif]-->
                        
	</head>
	<body style="">
            <div id="wrappler">
                <!-- Navigation -->
        <nav class="navbar navbar-default navbar-static-top" role="navigation" style="margin-bottom: 0">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="index.html">SB Admin v2.0</a>
            </div>
            <!-- /.navbar-header -->

            <div class="navbar-default sidebar" role="navigation">
                <div class="sidebar-nav navbar-collapse">
                    <ul class="nav" id="side-menu">
                        <li class="sidebar-search">
                            <div class="input-group custom-search-form">
                                <input type="text" class="form-control" placeholder="Search...">
                                <span class="input-group-btn">
                                <button class="btn btn-default" type="button">
                                    <i class="fa fa-search"></i>
                                </button>
                            </span>
                            </div>
                            <!-- /input-group -->
                        </li>
                        <li>
                            <a href="index.php"><i class="fa fa-dashboard fa-fw"></i> Dashboard</a>
                        </li>
                        <li>
                            <a href="personhtml.php"><i class="fa fa-table fa-fw"></i> SQL Table</a>
                        </li>
                        <li>
                            <a href="personjson.php"><i class="fa fa-table fa-fw"></i> JSON Table </a>
                        </li>
                        <li>
                            <a href="cardstate.php"><i class="fa fa-edit fa-fw"></i> Card Statement </a>
                        </li>
                        <li>
                            <a href="listtransactions.php"><i class="fa fa-wrench fa-fw"></i> Transactions </a>
                        </li>
                        <li>
                            <a href="searchnews.php"><i class="fa fa-sitemap fa-fw"></i> Search News </a>
                        </li>
                    </ul>
                </div>
                <!-- /.sidebar-collapse -->
            </div>
            <!-- /.navbar-static-side -->
        </nav>
                <div id="page-wrapper">
                    <h3>Information about SQL people.</h3>
                    <table class="table table-bordered">
                            <tbody>
                                    <tr class="bg-primary">
                                            <td><b>Name</b></td>
                                            <td><b>Location</b></td>
                                            <td><b>Telephone</b></td>
                                            <td><b>E-mail</b></td>
                                    </tr>
                            <?php while($row = mysqli_fetch_array($result)) { ?>
                                    <tr class="bg-info">
                                            <td><?php echo $row["firstname"]." ".$row["lastname"]; ?></td>
                                            <td><?php echo $row["city"].", ".$row["country"]; ?></td>
                                            <td><?php echo $row["telephone"]; ?></td>
                                            <td><?php echo $row["email"]; ?></td>
                                    </tr>
                            <?php } ?>
                            </tbody>
                    </table>
                </div>
                <!-- /#page-wrapper -->
            </div>
            <!-- /#wrapper -->
            
            <!-- jQuery -->
    <script src="../bower_components/jquery/dist/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="../bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

    <!-- Metis Menu Plugin JavaScript -->
    <script src="../bower_components/metisMenu/dist/metisMenu.min.js"></script>

    <!-- Morris Charts JavaScript -->
    <script src="../bower_components/raphael/raphael-min.js"></script>
    <script src="../bower_components/morrisjs/morris.min.js"></script>
    <script src="../js/morris-data.js"></script>

    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
	</body>
</html>
<?php mysqli_close($link); ?>